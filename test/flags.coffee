## flags.coffee

expect=require 'expect.js'
{render,renderable,normalizeArgs,comment,doctype,html,head,title,link,meta,script,body,section,main,aside,div,span,p,q,nav,header,footer,h1,h2,h3,h4,a,img,form,input,textarea,label,button,select,option,fieldset,ol,ul,li,table,tr,th,td,em,strong,b,i,blockquote,text,raw,tag,iframe,hr,br,coffeescript}=require 'double-macchiato'

# Tests.
describe 'flags',->
	it 'should render attributes without values',->
		expect render ->div contenteditable:yes,'Foo'
		.to.equal '<div contenteditable>Foo</div>'
	it '''shouldn't render when false''',->
		expect render ->button disabled:no,'Foo'
		.to.equal '<button>Foo</button>'
