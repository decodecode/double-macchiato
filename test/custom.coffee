# Test custom tag.

expect=require 'expect.js'
{render,tag,input,normalize_args}=require '..'

describe '"tag" helper',->
	it 'should render',->
		expect render ->tag 'custom'
		.to.equal '<custom></custom>'
	it 'should render empty given null content',->
		expect render ->tag 'custom',null
		.to.equal '<custom></custom>'
	it 'should render with attributes',->
		expect render ->tag 'custom',foo:'bar',ping:'pong'
		.to.equal '<custom foo="bar" ping="pong"></custom>'
	it 'should render with attributes and content',->
		expect render ->tag 'custom',foo:'bar',ping:'pong','zag'
		.to.equal '<custom foo="bar" ping="pong">zag</custom>'

describe 'custom tag-like',->
	text_input=->
		{attrs,contents}=normalize_args arguments
		attrs.type='text'
		input attrs,contents

	it 'should render',->
		expect render ->text_input()
		.to.equal '<input type="text">'
	it 'should accept a selector and attributes',->
		expect render ->text_input '.form-control',placeholder:'Beep'
		.to.equal '<input class="form-control" placeholder="Beep" type="text">'
