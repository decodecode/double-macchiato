# Escaping tests.

expect=require 'expect.js'
{render,raw,escape,h1,input}=require '..'

describe 'Auto escaping',->
	describe 'a script tag',->
		it "adds HTML entities for sensitive characters",->
			expect render ->h1 '''<script>alert('"owned" by c&a &copy;')</script>'''
			.to.equal "<h1>&lt;script&gt;alert('&quot;owned&quot; by c&amp;a &amp;copy;')&lt;/script&gt;</h1>"
	it 'escapes tag attributes',->
		expect render ->input name: '"pwned'
		.to.equal '<input name="&quot;pwned">'
	it 'does not escape single quotes in tag attributes',->
		expect render ->input name: "'pwned"
		.to.equal '<input name="\'pwned">'

describe '"raw" filter',->
	it 'prints sensitive characters verbatim',->
		expect render ->raw "<script>alert('on purpose')</script>"
		.to.equal "<script>alert('on purpose')</script>"

	describe 'combined with the escape filter',->
		it 'gives the author granular control of escaping',->
			expect render ->raw "<script>alert('#{escape 'perfect <3'}')</script>"
			.to.equal "<script>alert('perfect &lt;3')</script>"
