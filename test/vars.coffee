## vars.coffee
## Tests for variables.

expect=require 'expect.js'
{render,h1}=require '..'

describe 'Context data',-> #?// Ugh, weird usage, explain!
	it 'is an argument to the template function',->
		template=({foo})->h1 foo
		expect render template,foo:'bar'
		.to.equal '<h1>bar</h1>'
describe 'Local vars',->
	it 'are in the template function closure',-> #?// Nothing special?
		v='foo'
		template=->h1 "dynamic: #{v}"
		expect render template
		.to.equal '<h1>dynamic: foo</h1>'
		v='bar'
		expect render template
		.to.equal '<h1>dynamic: bar</h1>'
