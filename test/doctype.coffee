## Test doctype.

expect=require 'expect.js'
{render,doctype}=require '..'

describe 'doctype',->
	it 'should just render HTML5 doctype',->
		expect render ->doctype undefined
		.to.equal '<!DOCTYPE html>'
