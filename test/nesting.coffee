## nesting.coffee
## Tests for nesting.

expect=require 'expect.js'
{render,renderable,a,div,p,raw,span,strong}=require '..'

describe 'Nested templates',->
	#?// From TC. I don't understand what's this for. Scrap it,replace with real DM based helpers.
	it 'render in the same output',->
		user=first:'Huevo',last:'Bueno'
		helper=(user)->p "#{user.first} #{user.last}"
		template=(user)->
			div ->helper user
		expect render template,user #?// render args?! Why?
		.to.equal '<div><p>Huevo Bueno</p></div>'
describe 'Partials',->
	it 'work',->
		partial=renderable ->p 'Bar'
		expect render ->
			p 'Foo'
			raw partial()
			p 'Baz'
		.to.equal '<p>Foo</p><p>Bar</p><p>Baz</p>'
describe 'renderable decorator',->
	it 'makes a template directly callable',->
		template=renderable (letters)->
			for letter in letters
				div letter
		expect template ['a','b','c']
		.to.equal '<div>a</div><div>b</div><div>c</div>'
	it 'supports composition with renderable and non-renderable helpers',->
		renderable_helper=renderable (user)->span user.first
		helper=(user)->span user.last
		template=renderable (user)->
			div ->
				raw renderable_helper user
				helper user
		expect template first:'Huevo',last:'Bueno'
		.to.equal '<div><span>Huevo</span><span>Bueno</span></div>'
###?//
describe 'render',->
	describe 'nested in a template',->
		it 'returns the nested template without clobbering the parent result',->
			expect render ->
				p ->
					raw "This text could use #{render ->strong ->a href: '/','a link'}."
			.to.equal '<p>This text could use <strong><a href="/">a link</a></strong>.</p>'
	it 'doesn\'t modify the attributes object',->
		d=id:'foobar',class:'myclass',href:'http://example.com'
		expect render ->
			p ->
				a d,"link 1"
				a d,"link 2"
		.to.equal '<p><a id="foobar" class="myclass" href="http://example.com">link 1</a><a id="foobar" class="myclass" href="http://example.com">link 2</a></p>'
###
