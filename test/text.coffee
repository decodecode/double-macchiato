## Text helper tests.

expect=require 'expect.js'
{render,renderable,text,h1}=require '..'

describe 'Text or "text" helper',->
	it 'renders text verbatim',->
		expect renderable(text) 'foobar'
		.to.equal 'foobar'
	it 'renders numbers',->
		expect renderable(text) 1
		.to.equal '1'
		expect renderable(text) 0
		.to.equal '0'
	it 'is assumed when it is returned from contents',->
		expect render ->h1 ->'hello world'
		.to.equal '<h1>hello world</h1>'
		expect render ->h1 '.title',->'hello world'
		.to.equal '<h1 class="title">hello world</h1>'
		expect render ->h1 class:'title',->'hello world'
		.to.equal '<h1 class="title">hello world</h1>'
		expect render ->h1 '.title',->text 'hello world'
		.to.equal '<h1 class="title">hello world</h1>'
	if 'throws when content undefined',->
		expect render ->text undefined #?// Why is this an error? Other elements allow undefined. Default to empty string instead?
		.to.throw /Rendering of undefined failed: TypeError/
