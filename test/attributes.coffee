## attributes.coffee
## Unit tests for rendering attributes.

expect=require 'expect.js'
{render,a,br,div}=require '..'

describe 'Attributes',->
	describe 'Object parameter',->
		it 'renders the corresponding HTML attributes',->
			expect render ->a href:'/',title:'Home'
			.to.equal '<a href="/" title="Home"></a>'
	describe.skip 'Boolean true value',-> #?// No! Invalid. Broken.
		it 'is replaced with the attribute name.  Useful for attributes like disabled',->
			expect render ->br foo:yes,bar:true
			.to.equal '<br bar="bar" foo="foo">'
	describe.skip 'Boolean false value',->
		it 'is omitted',-> #?// Huh? Pro'ly broken.
			expect render ->br foo:no,bar:false
			.to.equal '<br>'
	describe.skip 'null and undefined',->
		it 'renders just the attribute name',-> #?// WTF? These should really be omitted!
			expect render ->br foo:null,bar:undefined
			.to.equal '<br bar foo>'
	describe 'String value',->
		it 'is used verbatim',->
			expect render ->br foo:'true',bar:'str'
			.to.equal '<br bar="str" foo="true">'
	describe 'Number value',->
		it 'is stringified',->
			expect render ->br foo:42,bar:3.14
			.to.equal '<br bar="3.14" foo="42">'
	describe 'Array value',->
		it 'renders as comma separated',->
			expect render ->br foo:[1,2,3]
			.to.equal '<br foo="1,2,3">'
	describe 'data attribute',->
		it 'expands',->
			expect render ->br data:{name:'Name',value:'Value'}
			.to.equal '<br data-name="Name" data-value="Value">'
	describe.skip 'Nested hyphenated attribute',-> #?// Huh?
		it 'renders',->
			expect render ->
				div 'on-x':'foo',->
					div 'on-y':'bar'
			.to.equal '<div on-x="foo"><div on-y="bar"></div></div>'
