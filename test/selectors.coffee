## Selectors tests.

expect=require 'expect.js'
{render,div,img}=require '..'

describe 'Selectors',->
	describe 'id selector',->
		it 'sets the id attribute',->
			expect render ->div '#myid','foo'
			.to.equal '<div id="myid">foo</div>'
		it 'must be greater than length 1',->
			expect render ->div '#'
			.to.equal '<div>#</div>'
	describe 'one class selector',->
		it 'adds an html class',->
			expect render ->div '.foo','foo'
			.to.equal '<div class="foo">foo</div>'
	describe 'and a class attribute',->
		it 'prepends the selector class',->
			expect render ->div '.foo','class':'bar','foo'
			.to.equal '<div class="foo bar">foo</div>'
	describe 'multi-class selector',->
		it 'adds all the classes',->
			expect render ->div '.foo.bar.baz','foo'
			.to.equal '<div class="foo bar baz">foo</div>'
	describe 'with an id and classes, separated by spaces',->
		it 'adds ids and classes with minimal whitespace',->
			expect render ->div '#myid .foo .bar '
			.to.equal '<div class="foo bar" id="myid"></div>'
	describe 'without contents',->
		it 'still adds attributes',->
			expect render ->img '#myid.foo',src:'/pic.png'
			.to.equal '<img class="foo" id="myid" src="/pic.png">'
