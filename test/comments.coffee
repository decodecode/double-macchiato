# Comment tests.

expect=require 'expect.js'
{render,comment}=require '..'

describe 'Comment',->
	it 'renders HTML <!--comments-->',->
		expect render ->comment "Foo"
		.to.equal '<!--Foo-->'
