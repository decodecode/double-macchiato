# Double Macchiato

## Constants.

	elements=

## HTML5 elements requiring a closing tag.
NB: the "var" element is out for obvious reasons, use "tag 'var'" instead.

		regular:'a abbr address article aside audio b bdi bdo blockquote body button canvas caption cite code colgroup data datalist dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 head header hgroup html i iframe ins kbd label legend li main map mark menu meter nav noscript object ol optgroup option output p picture pre progress q rp rt rtc ruby s samp section select small slot span strong sub summary sup table tbody td template textarea tfoot th thead time title tr u ul video'.split ' '
		raw:'script style'.split ' '

## Self-closing HTML5 elements.

		empty:'area base br col command embed hr img input keygen link menuitem meta param source track wbr'.split ' '

## Boolean attributes.

	flags='async autofocus autoplay checked controls contenteditable default defer disabled hidden ismap loop multiple novalidate open readonly required reversed scoped seamless selected spellcheck wrap'.split ' '

## Global buffer.

	_children=undefined

## APIs.

	render=exports.render=(template,args...)->(renderable template) args...
	renderable=exports.renderable=(template)-> #?//,buffer=undefined)->
		return (args...)->


			prev=swap_buffer [] #?//_children or=[] #?// Why not swap_buffer?
			template.apply exports,args #?// What's the apply for? Explain.
			r=swap_buffer prev
			render_children_to_html r


	swap_buffer=exports.swap_buffer=(buf)->
		prev=_children
		_children=buf
		prev

	render_children_to_html=(ch)->if Array.isArray ch then (render_html e for e in ch).join '' else ch or ''

	render_html=(e)->
		try
			if typeof e in ['string','number'] then e
			else if e.type is 'self_closing_tag' then "<#{e.element}#{render_attrs e.attributes}>"
			else "<#{e.element}#{render_attrs e.attributes}>#{render_children_to_html e.contents}</#{e.element}>"
		catch err


			throw new Error "Rendering of #{JSON.stringify e} failed: #{err}"

	render_attr=(name,value)->
		if name in flags
			return if value is true then " #{name}" else ''
		if name is 'data' and typeof value is 'object'
			return (render_attr "data-#{k}",v for own k,v of value).join ''
		else
			return " #{name}=\"#{escape value.toString()}\""

	render_attrs=(obj)->
		(render_attr name, value for own name,value of obj)
		#?//.filter (e)->e? #?// Unused, and questionable?
		.sort()
		.join ''

	render_contents=exports.render_contents=(contents,args...)->
		if typeof contents is 'function'
			previous=swap_buffer [] #?// Yuck!
			r=contents args...
			if not _children.length then _children.push r #?// Case function just returns text?
			return swap_buffer previous
		if typeof contents is 'object' #?// Like what?
			return contents
		else #?// Must be string? Stringified?
			return contents? and escape(contents.toString()) or ''

	is_selector=exports.is_selector=(string)->
		string.length > 1 and string.charAt(0) in ['#','.']

	parse_selector=exports.parse_selector=(selector)->
		id=undefined
		classes=[]
		for token in selector.split '.'
			token=token.trim()
			if id
				classes.push token
			else
				[klass,id]=token.split '#'
				classes.push token unless klass is ''
		return {id,classes}

//? Auto convert attribute names from camel/snake-case to kebub-case?! Especially for data attributes, like jQuery does!

	normalize_args=exports.normalize_args=(args)->
		attrs={}
		selector=undefined
		contents=undefined
		for arg,index in args when arg?
			switch typeof arg
				when 'string'
					if index is 0 and is_selector arg
						selector=arg
						parsed=parse_selector arg
					else
						contents=arg
				when 'function','number','boolean'
					contents=arg
				when 'object'
					if arg.constructor == Object
						attrs=arg
					else
						contents=arg
				else
					contents=arg
		if parsed?
			{id,classes}=parsed
			attrs.id=id if id?
			if classes?.length
				if attrs.class then classes.push attrs.class
				attrs.class=classes.join ' '
		return {attrs,contents,selector}

	tag=exports.tag=(t,args...)->
		{attrs,contents}=normalize_args args
		_children.push c=
			element:t
			attributes:attrs
			contents:render_contents contents
		return c

	raw_tag=(tagName,args...)->
		{attrs,contents}=normalize_args args
		_children.push element:tagName,attributes:attrs,contents:contents #?// Rename and use clean re/destructuring

	self_closing_tag=(tag,args...)->
		{attrs,contents}=normalize_args args
		if contents then throw new Error "Error: <#{tag}> must not have content; attempted to nest #{contents}."
		_children.push element:tag,attributes:attrs,type:'self_closing_tag'

	comment=exports.comment=(text)->_children.push "<!--#{escape text}-->"
	doctype=exports.doctype=->_children.push '<!DOCTYPE html>'
	text=exports.text=(s)->_children.push s #?// escape?!!!


	raw=exports.raw=(s)->
		return unless s?
		_children.push s


	coffeescript=exports.coffeescript=(fn)->
		_children.push """<script type="text/javascript">(function() {
			var __slice = [].slice,
					__indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
					__hasProp = {}.hasOwnProperty,
					__extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };
			(#{fn.toString()})();
		})();</script>""".replace /\t/g,''

## Filters

	escape=exports.escape=(text)->
		text.toString().replace /&/g,'&amp;'
			.replace /</g,'&lt;'
			.replace />/g,'&gt;'
			.replace /"/g,'&quot;' # No neet to escape single quote (') because attributes always use double quotes (").

## Plugins

	use=exports.use=(plugin)->plugin exports

## Bind all element names to exported functions.
//? on the prototype for pretty stack traces

	for t in elements.regular
		do (t)->exports[t]=(args...)->tag t,args... # ("do" provides closure so each "t" exists for bound callbacks.)
	for t in elements.raw
		do (t)->exports[t]=(args...)->raw_tag t,args...
	for t in elements.empty
		do (t)->exports[t]=(args...)->self_closing_tag t,args...
