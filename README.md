# Double Macchiato
> CoffeeScript-based DSL for generating HTML.

CoffeeScript is such a clean language, that instead of embedding code in a HTML templating (ie, domain specific) language (DSL), like HAML or Pug (neé Jade), Double Macchiato can invert that embedding, sacrificing purity a little, to provide such a DSL as a CoffeeScript library, gaining the full expressive power of a programming language.

Eg:
```
header '#toolbar',->
	if config.searchable
		label for:'search','Search'
		input '#search',placeholder:'Keywords'
```

## Installation

	npm install double-macchiato

or

	npm install git+https://gitlab.com/decodecode/double-macchiato

## API

### Features

1. Selectors
2. Attributes
3. data- attributes
4. Boolean attributes
4. Tags
5. Text, escaping
5. Rendering
6. Helpers

## Development

Clone, fork…

### Test

Server-side unit testing:

	npm test

(Client-side tests — not yet, work in progress.)

### Roadmap

1. Boolean attributes: broken?
1. Helper helpers: APIs to simplify creating helpers, DSLs…
2. Documentation
2. Testing coverage

### Contributing

[GitLab](https://gitlab.com/decodecode/double-macchiato).

## Authors

Standing on lots of shoulders…
[https://decodecode.net/elitist/2014/07/coffeecup/],
[https://decodecode.net/elitist/2015/05/hacking-teacup/].

## Changelog

### Next, currently in development

…

### 0.2.1, released 2019-07-02

- GitHub→GitLab.

### 0.2.0, published 2018-01-09

- Basic DSL for "HTML5" (common? best?) practices…
